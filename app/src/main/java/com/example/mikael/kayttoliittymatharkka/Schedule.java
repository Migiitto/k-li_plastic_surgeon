package com.example.mikael.kayttoliittymatharkka;

import android.app.TimePickerDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CalendarView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.Calendar;

public class Schedule extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule);
        final CalendarView calendar = (CalendarView) findViewById(R.id.calendarView);
        calendar.setClickable(true);
        calendar.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(CalendarView calendarView, int i, int i1, int i2) {
                TimePickerDialog timePicker;
                timePicker = new TimePickerDialog(Schedule.this, new TimePickerDialog.OnTimeSetListener(){
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute){
                        Toast.makeText(getApplicationContext(),"Tapaaminen lisätty kalenteriin!",Toast.LENGTH_LONG);
                        System.exit(1);
                    }
                },12,00,true);
                timePicker.show();

            }
        });





    }
}
