package com.example.mikael.kayttoliittymatharkka;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class SearchActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        final ListView listView2 = (ListView) findViewById(R.id.listView2);
        String[] values = new String[] {"Anna Asiakas","Anna Tree","Anna Janna"};
        final ArrayList<String> list = new ArrayList<String>();
        for (int i = 0; i < values.length; ++i){
            list.add(values[i]);
        }
        final ArrayAdapter adapter = new ArrayAdapter(this,
                android.R.layout.simple_list_item_1,list);
        listView2.setAdapter(adapter);
    }
}
