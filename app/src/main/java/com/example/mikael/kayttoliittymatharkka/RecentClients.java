package com.example.mikael.kayttoliittymatharkka;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

public class RecentClients extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recent_clients);
        final ListView listView = (ListView) findViewById(R.id.listView);
        String[] values = new String[] {"Anna Asiakas","Belle Miljååna","Kikki Maus","Liisa Lista",
        "Juttu Ja", "Pär Jonnesson","Kati-Leena Jenneri","Moti Nimiä","Teija Testi","Käyttö Liittymä"};
        final ArrayList<String> list = new ArrayList<String>();
        for (int i = 0; i < values.length; ++i){
            list.add(values[i]);
        }
        final ArrayAdapter adapter = new ArrayAdapter(this,
                android.R.layout.simple_list_item_1,list);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> adapter, View v, int position,long id){
                Object item = adapter.getItemAtPosition(position);
                Intent intent = new Intent(RecentClients.this, MainMenuActivity.class);
                startActivity(intent);
            }
        });




    }
    public void goFind(View view) {
        startActivity(new Intent(this, SearchActivity.class));
    }
    public void goNew(View view) {
        startActivity(new Intent(this, NewClientActivity.class));
    }
}
