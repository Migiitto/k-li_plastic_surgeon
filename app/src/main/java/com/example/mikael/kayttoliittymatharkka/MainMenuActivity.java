package com.example.mikael.kayttoliittymatharkka;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainMenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);
    }
    public void goPlan(View view) {
        startActivity(new Intent(this, PlanActivity.class));
    }
    public void goOperation(View view) {
        startActivity(new Intent(this, OperationActivity.class));
    }
    public void goCheckup(View view) {
        startActivity(new Intent(this, CheckupActivity.class));
    }
    public void goSchedule(View view) {
        startActivity(new Intent(this, Schedule.class));
    }
    public void goExit(View view) {
        System.exit(1);
    }
}
